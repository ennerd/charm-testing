Charm/Testing
=============

A command line tool for running quickly writing regression and unit tests without
any boilerplate.


Writing a test
--------------

Create a folder `charm-tests/` in your project root. Tests are contained in
simple PHP files which return an array like this:

```
<?php return [
    /**
     * First array item is a string that describes the test.
     */
    "Test description",

    /**
     * Second item is a function which performs the testing. The function
     * must return data which will be compared to the expected result.
     */
    function($data) {
        // perform your test and return a value
        return strtoupper($args);
    },

    /**
     * The next items are arrays of testing criteria and their expected result
     */
    [ "hello world",        "HELLO WORLD" ],        // succeeds
    [ "blåbærsyltetøy",     "BLåBæRSYLTETøY" ],     // succeeds
    [ "blåbærsyltetøy",     "BLÅBÆRSYLTEDØY" ],     // fails, but is the correct result

];


The test report
---------------

The test report contains the date and the result of all the tests with a nice
side-by-side diff highlighting the difference between the expected result and
the actual result.

```

```


Running the tests
-----------------

Run the function `vendor/bin/charm-testing` and you will see the tests being performed.
The output is a markdown file that you can save for future reference.


Simple Automation Example
-------------------------

The `charm-testing` command can easily be automated in various contexts, because it
provides an exit code which indicates the percentage of failed tests rounded up.

 * If no tests failed, the exit code is `0`.
 * If all tests failed, the exit code is `100`.
 * If 1 test failed, the exit code is `1` (or `100` if you only have
   one test. ;-)

Invoking the tests from PHP:

```php
exec('exec /usr/bin/env vendor/bin/charm-testing', $output, $exitCode);
if ($exitCode > 0) {
    // some tests failed!
}
```

Handling failed tests from the command line:

```bash
./vendor/bin/charm-testing || echo "Some tests failed!"
```

or

```bash
if [[ ! $(./vendor/bin/charm-testing) ]]; then
    echo "Some tests failed"
fi
```
