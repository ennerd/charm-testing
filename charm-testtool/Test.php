<?php
namespace Charm;

use Charm\TestTool\Runner;

/**
 * Entrypoint for performing tests while running tests.
 */
final class Test {
    private static function instance(): Runner {
        return Runner::instance();
    }

    private static function log(bool $successful, string $description, mixed $expected=null, mixed $received=null): void {
        self::instance()->log
        self::instance()->assertionFail(
    }

    public static function expect($value, $result, string $description=null): void {
        
    }

    public static function assert($truthy, string $description=null): void {
        self::log(!!$truthy, 

        if (!$truthy) {
            self::assertionFail("assert", $description, $value);
        } else {
            self::assertionSuccess("assert", $description, $value);
        }
    }

    public static function equals($result, $expected, string $description=null): void {
        if (gettype($result) !== gettype($expected)) {
            self::assertionFail("equals", $description, $result);
        }
        if ($result == $expected) {
    }
}
