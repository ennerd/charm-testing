<?php
namespace Charm;

use TestTool\Runner;

class TestTool extends Terminal {

    private string $root;
    private string $testsPath;
    private Terminal $terminal;

    public function __construct(string $root) {
        parent::__construct(STDOUT);
        $this->root = realpath($root);
        if (!$this->root) {
            throw new \Exception("Path '$root' not found");
        }
        if (is_dir($this->testPath = $this->root.'/charm-tests'));
        elseif (is_dir($this->testPath = $this->root.'/tests'));
        else $this->fail(255, "No test directory found in {$this->root}/charm-tests or {$this->root}/tests");

    }

    public function runAll(): void {
        $testRunners = [];
        foreach (glob($this->testPath.'/*.php') as $path) {
            $runner = new Runner($path);
            $runner->start();
            $testRunners[] = $runner;
        }
    }

    private function fail(int $exitCode, string $reason): void {
        $this->write("Error: <!bold>$reason<!>\n");
        exit($exitCode);
    }

}
