<?php
namespace Charm\Testing;

class ComposerTester extends TestProvider {

    private $root, $addError, $addWarning, $addSuccess;

    public function __construct(string $root, object $options) {
        $this->root = $root;
        $this->options = $options;
    }

    public function run(): void {
        global $be_brief;

        h2("composer.json checks");
        if (!is_file($this->root."/composer.json")) {
            ($this->options->addWarning)($this->root.'/composer.json', "No composer.json file found");
            $be_brief or path_warn($this->root.'/composer.json', "Not found");
        }
        $composer = json_decode(file_get_contents($this->root.'/composer.json'));
    }

}
