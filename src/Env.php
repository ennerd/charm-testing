<?php
namespace Charm\Testing;

class Env {

    public static function homePath(): string {
        $uid = posix_getuid();
        $shell_user = posix_getpwuid($uid);
        $home = $shell_user['dir'];
        return $home;
    }

    public static function varPath(): string {
        $varPath = self::homePath().'/.charm-testing';
        if (!is_dir($varPath)) {
            mkdir($varPath, 0700);
        }
        return $varPath;
    }

}
