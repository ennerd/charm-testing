<?php
use Charm\Terminal;
use function Charm\php_encode;

define("NO_YIELD", 1);
define("NO_RECURSE", 2);

// colors
$cGOOD = "<!lime>";
$cNEUTRAL = "<!silver>";
$cNOTICE = "<!olive>";
$cBAD = "<!red>";
$cWARNING = "<!yellow>";
$cHIGHLIGHT = "<!aqua>";
$cSTATUS = "<!purple>";
$cGFX = "<!grey>";
$cSTDOUT = "<!aqua>";
$cSTDERR = "<!fuchsia>";
$cOUTPUTMARK = "<!green>";
$cH1 = "<!white>";
$cH2 = "<!white>";

$outStack = [];

$term = new Charm\Terminal(STDOUT);
$cache = Charm\Testing\Cache::instance();

/**
 * Find the project we're testing
 */
$root = find_project_dir();

/**
 * Find prefixes
 */
getopt('hltgbq', ['help', 'no-linting', 'no-tests', 'no-git', 'flush-cache', 'brief', 'quiet', 'summary'], $prefixIndex);
$prefixes = [];
for (; isset($argv[$prefixIndex]); $prefixIndex++) {
    $prefixes[] = $argv[$prefixIndex];
}

/**
 * Help argument?
 */
if (getopt('h', ['help'])) {
    $command = basename($argv[0]);

    stdout(<<<CHELP
        Usage: $cHIGHLIGHT$command<!> [options] [...filter]

        [filter]            One or more test filters which can be used to limit which test
                            files are being run.

        For example, running $cHIGHLIGHT$command 01-<!> will recursively search for PHP files
        matching the pattern $cHIGHLIGHT./charm-tests/01-*.php<!>. See below under Filters for
        more information.

        <!white>Options<!>
            -h, --help          Show this help text
            -l, --no-linting    Skip linting of source files
            -t, --no-tests      Skip running tests from charm-tests/
            -g, --no-git        Don't check for uncommited git repositories in vendor/
                --flush-cache   Delete all cached entries (stored in ~/.charm-testing/cache)
            -b, --brief         Be less verbose
            -q, --quiet         Don't output anything
                --summary       Only output the summary


        <!white>Filters<!>
            You can restrict which tests will be run by providing one or more prefixes. For example
            running $cHIGHLIGHT$command 01<!> will execute all tests which start with the string 01
            in their basename or path. In other words the prefix "01" will match the following:

            ./charm-testing/01-example.php
            ./charm-testing/01-group-of-tests/some-test.php


        <!white>Test types<!>
            There are several ways to write tests, and which testing methodology you want to use
            depends on your use case:

             * <!underline>Testing of output<!> (stdout and stderr)
               This is easiest done by running the test and piping the output to a file.

             * <!underline>Assertions<!> (via the php language construct `assert();`)
               The assert() function should be used throughout the code base. It is automatically
               removed in production and has zero cost. This tool will enable those assertions
               when running test scripts.

             * <!underline>Exit code<!> (by calling `exit(123);` where 123 is a non-zero error code)
               Any fatal error or unhandled exception will result in a non-zero exit code.

             * <!underline>Error log<!>
               After each test, the error log is checked. If it contains any errors, the test is
               considered failed.


            <!white underline>Testing of output<!>
            There are two ways to write output validating tests; writing `.phpt`-files, in the
            same format as the PHP engines unit tests:

            ./charm-tests/001.phpt
            ```
            --TEST--
            Trivial "Hello World" test
            --FILE--
            <!php><?php echo "Hello World"; ?><!>
            --EXPECT--
            Hello World
            ```

            Or by creating an extra file with the suffix $cHIGHLIGHT.STDOUT<!> or $cHIGHLIGHT.STDERR<!>

            ./charm-tests/example-test.php
            ```
            <!php><?php echo "Hello World";<!>
            ```

            Then run$cHIGHLIGHT php example-test.php > example-test.php.STDOUT<!> to generate an output
            file which will be used for validation.

            To create a test for stderr you can run$cHIGHLIGHT php example-test.php 2> example-test.php.STDERR<!>.

            Wildcards in .STDOUT and .STDERR
            Some times your output contains numbers, dates or variable output. You can edit the test file
            and use * and ? as wildcards. * will match zero or more characters, and ? will match a single
            character. If you need to match one or more characters you should use ?* in combination.


            <!white underline>Assertions<!>
            If your codebase contains assertions (it should), then running tests will automatically
            enable those assertion tests. They are generally NOT enabled in web servers, so they are
            a clever tool to test output.

            The general practice is to write assert() statements wherever you make assumptions about some
            state:

            ```
            function get_username() {
                global \$user;
                // This has ZERO effect on performance in production
                assert(\$user, "The \$user global is false or null");
            }

            The test files are plain and simple PHP files. Example:

            ```
            <!php><?php

            // See https://www.php.net/assert for documentation
            assert(
                class_exists(SomeClass::class),
                "Class 'SomeClass' does not exist"
            );<!>
            ```


            <!white underline>Exit Code<!>
            If at any time in your script you call `exit(2);`, the test will be considered a failure. In
            general, we recommend instead that you call `assert(false, "Some condition failed");` - because
            this way you can provide a description of the error.


            <!white underline>Error Log<!>
            If anything is written to the error log while the test is running, it is considered an error.


        CHELP);
    exit(0);
}

$only_summary = false;
if (getopt('', ['summary'])) {
    $only_summary = true;
}

$be_brief = false;
if (getopt('b', ['brief'])) {
    $be_brief = true;
}
$be_quiet = false;
if (getopt('q', ['quiet'])) {
    $be_quiet = true;
}

if ($only_summary) {
    $be_quiet = true;
}


/**
 * Flush cache argument
 */
if (getopt('', ['flush-cache'])) {
    out("Flushing cache...");
    $cache->flush();
    out("<!green>done.<!>\n");
    exit(0);
}

/**
 * Perform the testing
 */
if (count($prefixes) === 0) {
    out_h1("Charm/Testing report");
    out(" Tests started ".gmdate('Y-m-d H:i:s')." GMT\n\n");
}

/**
 * List of failed tests
 */
$errorList = [];

/**
 * Statistics
 */
$successes = 0;
$warnings = 0;
$errors = 0;
$notices = 0;

/**
 * Start the tests
 */
if (!getopt('t', ['no-tests']) && is_dir($root.'/charm-tests')) {

    /**
     * Tests have been filtered by prefix. We'll take out all tests
     * that does not match the prefix in either basename or path
     * relative to charm-tests/
     */
    if (count($prefixes) > 0) {
        $testFiles = [];
        foreach (find_test_files($pathPrefix = $root.'/charm-tests') as $testFile) {
            $testFullPath = substr($testFile, strlen($pathPrefix) + 1);
            foreach ($prefixes as $prefix) {
                if (strpos($testFullPath, $prefix) !== false) {
                    $testFiles[] = $testFile;
                    continue 2;
                }
            }
        }
    } else {
        $testFiles = iterator_to_array(find_test_files($root.'/charm-tests'));
    }

    if (count($testFiles) === 0) {
        $warnings++;
        $errorList[] = [ 'charm-tests/', 'No tests found', 'warning' ];
        goto skip_charm_tests;
    }

    foreach ($testFiles as $testFile) {

        $ext = strtolower(pathinfo($testFile, PATHINFO_EXTENSION));

        if ($ext === 'phpt') {
            $info = [];
            $fp = fopen($testFile, 'rb');
            $name = null;
            while (!feof($fp)) {
                $line = fgets($fp, 65536);
                if (substr($line, 0, 2) === '--' && substr(trim($line), -2) === '--') {
                    $name = substr(rtrim($line), 2, -2);
                    continue;
                } elseif ($name === null) {
                    $errorList[] = [ $testFile, "File must start with a line indicating section: --FILE--, --EXPECT-- or --TEST--", 'warning' ];
                    $warnings++;
                    continue 2;
                }
                if (key_exists($name, $info)) {
                    $info[$name] .= $line;
                } else {
                    $info[$name] = $line;
                }
            }
            if (empty($info['FILE'])) {
                $errorList[] = [ $testFile, "Missing --FILE-- section", 'warning' ];
                $warnings++;
                out(" ".mark_error()." Invalid test file <!underline>$testFile<!>: <!red>No --FILE-- section found<!>\n\n");
            } elseif (empty($info['EXPECT'])) {
                $errorList[] = [ $testFile, "Missing --EXPECT-- section", 'warning' ];
                $warnings++;
                out(" ".mark_error()." Invalid test file <!underline>$testFile<!>: <!red>No --EXPECT-- section found<!>\n\n");
            } elseif (empty($info['TEST'])) {
                $errorList[] = [ $testFile, "Missing --TEST-- section", 'warning' ];
                $warnings++;
                out(" ".mark_error()." Invalid test file <!underline>$testFile<!>: <!red>No --TEST-- section found<!>\n\n");
            } else {
                $filename = $testFile;
                $t = tempnam(sys_get_temp_dir(), 'charm-test');
                $testFile = $t.'.php';
                file_put_contents($testFile, $info['FILE']);
                if (0 !== run_test($testFile, [
                    'filename' => $filename,
                    'name' => trim($info['TEST']),
                    'stdout' => trim($info['EXPECT']),
                ])) {
//                    $errorList[] = [ $testFile, $info['TEST'], 'error' ];
//                    $errors++;
                }
            }
        } else {
            if (0 === run_test($testFile)) {
//                $successes++;
            }
/*
 else {
                $nameT = strtr(basename($testFile), [ '-' => ' ', '.php' => '' ] );
                $parts = [];
                $name = '';
                foreach (explode(" ", trim($nameT)) as $index => $part) {
                    if (trim($part) === '') {
                        continue;
                    }
                    if ($index === 0 && trim($part, '0123456789')==='') {
                        continue;
                    }
                    $parts[] = $part;
                }
                $name = implode(" ", $parts);
*/
                //FRODE$errorList[] = [ $testFile, $name, 'error' ];
    //            $errors++;
//            }
        }
    }
    skip_charm_tests:
}

if (!getopt('l', ['no-linting']) && count($prefixes) === 0) {

    out_h2("Linting files");

    out_start('linting');

    $lintStats = [
        'total-files' => 0,
        'successes' => 0,
        'fails' => 0,
        'unlinted' => 0,
    ];

    $info = json_decode(file_get_contents($root.'/composer.json'), true);
    $paths = [];
    foreach (['autoload', 'autoload-dev'] as $autoload) {
        if (!empty($info[$autoload]['psr-4'])) {
            foreach ($info[$autoload]['psr-4'] as $path) {
                $paths[] = $root.'/'.rtrim($path, '/\\');
            }
        }
        if (!empty($info[$autoload]['psr-0'])) {
            foreach ($info[$autoload]['psr-0'] as $path) {
                $paths[] = $root.'/'.rtrim($path, '/\\');
            }
        }
        if (!empty($info[$autoload]['files'])) {
            foreach ($info[$autoload]['files'] as $path) {
                $paths[] = $root.'/'.$path;
            }
        }
        if (!empty($info[$autoload]['classmap'])) {
            foreach ($info[$autoload]['classmap'] as $path) {
                $paths[] = $root.'/'.rtrim($path, '/\\');
            }
        }
    }
    if (!empty($info['include-path'])) {
        foreach ($info['include-path'] as $path) {
            $paths[] = $root.'/'.rtrim($path, '/\\');
        }
    }
    $currentNumber = 0;
    $lintedPaths = [];
    foreach (array_unique($paths) as $lintRoot) {
        walk_paths($lintRoot, function(string $path) use ($root) {
            global $cGOOD, $cNEUTRAL, $cNOTICE, $cBAD, $cWARNING, $cHIGHLIGHT, $cSTATUS, $cGFX, $cSTDOUT, $cSTDERR, $cOUTPUTMARK, $cH1, $cH2;
            global $lintStats, $be_brief;
            global $lintedPaths;
            if (isset($lintedPaths[$path])) {
                return;
            }
            $basename = basename($path);
            if (is_dir($path)) {
                if ($path === $root . DIRECTORY_SEPARATOR . "vendor") {
                    return false;
                }
                if ($basename[0] === ".") {
                    return false;
                }
                if (
                    file_exists($path . DIRECTORY_SEPARATOR . 'composer.json') ||
                    is_dir($path . DIRECTORY_SEPARATOR . '.git')
                ) {
                    return false;
                }
                return true;
            }

            $lintedPaths[$path] = true;
            $lintStats['total-files']++;
            tty_out("\r<!clear-line> $cHIGHLIGHT$path<!> (".$cSTATUS."linting...<!>)");
            $error = lint_file($path);
            tty_out("\r<!clear-line>");
            if ($error === null) {
                // not linted
                $lintStats['unlinted']++;
            } elseif ($error === '') {
//                $be_brief or out_path_success($path, "");
                $lintStats['successes']++;
            } else {
                out_path_error($path, explode("\n", trim($error))[0]);
                add_error($path, $error);
                $lintStats['fails']++;
            }

/*
            if (fnmatch("*.php", $path)) {
                tty_out("\r<!clear-line> $cHIGHLIGHT$path<!> (".$cSTATUS."linting...<!>)");
                $error = lint_file($path);
                if ($error !== null) {
                    tty_out("\r<!clear-line>");
//                    out(" ".mark_dot()." $cHIGHLIGHT$path<!> <!red>".explode("\n", trim($error))[0]."<!>\n");
                    out_path_error($path, explode("\n", trim($error))[0]);
                    add_error($path, $error);
                    $lintStats['fails']++;
                } else {
                    tty_out("\r<!clear-line>");
                    $lintStats['successes']++;
                }
            } else {
                tty_out("\r<!clear-line>");
                $be_brief or out_path_warning($path, "Not lintable");
//                $be_brief or out(" $cHIGHLIGHT$path<!> <!olive>Not lintable<!>\n");
                $lintStats['unlinted']++;
                add_warning($path, "Unexpected file '$basename' in autoloader path");
            }
*/
        });
    }

    // Lint other files in the project
    walk_paths($root, function($path) use ($root) {
        global $cGOOD, $cNEUTRAL, $cNOTICE, $cBAD, $cWARNING, $cHIGHLIGHT, $cSTATUS, $cGFX, $cSTDOUT, $cSTDERR, $cOUTPUTMARK, $cH1, $cH2;
        global $lintStats, $be_brief;
        global $lintedPaths;
        if (isset($lintedPaths[$path])) {
            return;
        }
        $basename = basename($path);
        if (is_dir($path)) {
            if ($path === $root . DIRECTORY_SEPARATOR . "vendor") {
                return false;
            }
            if ($basename[0] === ".") {
                return false;
            }
            if (
                file_exists($path . DIRECTORY_SEPARATOR . 'composer.json') ||
                is_dir($path . DIRECTORY_SEPARATOR . '.git')
            ) {
                return false;
            }
            return true;
        }
        $lintedPaths[$path] = true;
        $lintStats['total-files']++;
        tty_out("\r<!clear-line> $cHIGHLIGHT$path<!> (".$cSTATUS."linting...<!>)");
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $error = lint_file($path);
        tty_out("\r<!clear-line>");
        if ($error === null) {
            // not linted
            $lintStats['unlinted']++;
        } elseif ($error === '') {
//            $be_brief or out_path_success($path, "");
            $lintStats['successes']++;
        } else {
            out_path_error($path, explode("\n", trim($error))[0]);
            add_error($path, $error);
            $lintStats['fails']++;
        }
    });

    out_end('linting');

    if ($lintStats['total-files'] === 0) {
        $be_brief or out(" ".$cHIGHLIGHT."No files found in paths managed by composer.json<!>\n\n");
    } else {
        if (!$be_brief) {
            out_summary("Results from linting", [
                ['success', $lintStats['successes'], '{count} successes'],
                ['error', $lintStats['fails'], '{count} failed linting'],
                ['unlinted', $lintStats['unlinted'], '{count} files with unsupported file types'],
            ]);
        }
//        $be_brief or out("\n Found ".$cNEUTRAL.$lintStats['total-files']."<!> files. Of these files ".$cGOOD.$lintStats['successes']."<!> were successfully linted.\n\n");
/*
        $didOutput = false;
        if ($lintStats['fails'] > 0) {
            $didOutput = true;
//            out(" ".mark_error()." ".$cBAD.$lintStats['fails']."<!> failed linting.\n");
        }
        if ($lintStats['unlinted'] > 0) {
//            $didOutput = true;
//            $be_brief or out(" ".mark_dot()." ".$cNOTICE.$lintStats['unlinted']."<!> files could not be linted.\n");
        }
        if ($didOutput) {
            out("\n");
        }
*/
    }
}

if (!getopt('g', ['no-git']) && count($prefixes) === 0) {
    out_h2("Checking git repos recursively");

    $gitReposChecked = 0;
    $gitReposDirty = 0;

    out_start("git-repos");

    walk_paths($root, function(string $path) use (&$gitReposChecked, &$gitReposDirty) {
        global $cGOOD, $cNEUTRAL, $cNOTICE, $cBAD, $cWARNING, $cHIGHLIGHT, $cSTATUS, $cGFX, $cSTDOUT, $cSTDERR, $cOUTPUTMARK, $cH1, $cH2;
        global $be_brief;
        $basename = basename($path);
        if (is_dir($path)) {
            if ($basename[0] === ".") {
                return false;
            }
            if (is_dir($path . DIRECTORY_SEPARATOR . '.git')) {
                $gitReposChecked++;
                tty_out("\r<!clear-line>");
                tty_out(" ".$cHIGHLIGHT.$path."<!> ($cSTATUS"."checking...<!>)");
                if (is_git_clean($path)) {
                    tty_out("\r<!clear-line>", true);
                    $be_brief or out_path_success($path, "OK");
                } else {
                    $gitReposDirty++;
                    tty_out("\r<!clear-line>");
                    out_path_warning($path, "Dirty");
                    add_warning($path, "Repository is dirty");
                }
                return false;
            }
            return true;
        }
    });

    out_end("git-repos");

    if ($gitReposChecked === 0) {
        $be_brief or out_conclude_success("No child GIT repositories");
    } elseif ($gitReposDirty === 0) {
        $be_brief or out_conclude_success("No dirty child git repositories found");
    } else {
        $be_brief or out_conclude_warning("$gitReposDirty dirty git repos");
    }
}

if (count($prefixes) === 0) {
    out_h1("Summary of the results");

    if ($only_summary) {
        $be_quiet = false;
    }
    if (count($errorList) > 0) {
        $successCount = 0;
        $warningCount = 0;
        $errorCount = 0;
        foreach ($errorList as list($file, $desc, $type)) {
            if ($type === 'success') {
                ++$successCount;
            } elseif ($type === 'error') {
                ++$errorCount;
            } elseif ($type === 'warning') {
                ++$warningCount;
            }
        }
        foreach ($errorList as list($file, $desc, $type)) {
            if ($type === 'error') {
                $prefix = mark_error()." ".$cBAD.'ERROR<!>  ';
            } elseif ($type === 'warning') {
                $prefix = mark_warning()." ".$cNOTICE.'WARNING<!>';
            } elseif ($type === 'notice') {
                // not outputting notices in summary
                continue;
            } elseif ($type === 'success') {
                // not outputting successes in summary
                continue;
            } else {
                $prefix = mark_dot()." ".$cHIGHLIGHT.'OTHER<!>  ';
            }
            out(" $prefix <!white>$file<!>\n");
            if (trim($desc) !== '') {
                $desc = wordwrap($desc, 80);
                foreach (explode("\n", trim($desc)) as $line) {
                    out("            ".strtr($line, [ "\n" => $cOUTPUTMARK."⏎<!>\n" ])."\n");
                }
            }
        }
        if ($errorCount === 0 && $warningCount === 0) {
            $be_brief or out(" ".mark_success()." ".$cGOOD."SUCCESS<!> $successCount tests were successful\n");
        }
        out("\n");
    }

    if ($only_summary) {
        $be_quiet = true;
    }

    $total = $successes + $warnings + $errors;

    if ($warnings === 0 && $errors === 0 && $successes > 0) {
        out(" ".mark_success()." <!green>All $successes tests succeeded<!>\n\n");
    } elseif ($warnings === 0 && $errors === 0 && $successes === 0) {
        out(" ".mark_error()." <!red>No tests run<!>\n\n");
    } else {
        $parts = [];
        if ($errors !== 0) {
            $parts[] = "$cBAD$errors<!> errors";
        }
        if ($warnings !== 0) {
            $parts[] = "$cNOTICE$warnings<!> warnings";
        }
        if ($successes !== 0) {
            $parts[] = "$cGOOD$successes<!> successes";
        }

        if (count($parts) === 1) {
            $text = 'There were '.$parts[0].' during testing';
        } elseif (count($parts) === 2) {
            $text = 'There were '.$parts[0].' and '.$parts[1].' during testing';
        } elseif (count($parts) === 3) {
            $text = 'There were '.$parts[0].', '.$parts[1].' and '.$parts[2].' during testing';
        }

        $be_brief or out(" $text\n\n");
    }

    if ($errors === 0 && $warnings === 0) {
        out_conclude_success("Tests successful");
        exit(0);
    } elseif ($errors === 0 && $successes > 0) {
        out_conclude_warning("Tests successful with warnings");
        exit(0);
    } else {
        out_conclude_error("Tests failed");
        exit(1);
    }
} else {
    if ($errors > 0) {
        exit(1);
    }
    exit(0);
}

function lint_file(string $path): ?string {
    static
        $jsonLint = new \Seld\JsonLint\JsonParser(),
        $cssLint = null;

    if ($cssLint === null) {
        $cssLintProps = new \CssLint\Properties();
        $cssLintProps->setAllowedIndentationChars([' ', "\t"]);
        $cssLint = new \CssLint\Linter($cssLintProps);
    }

    $ext = pathinfo($path, PATHINFO_EXTENSION);
    switch ($ext) {
        case 'php':
            $result = shell_exec(PHP_BINARY.' -l '.escapeshellarg($path).' 2>&1 1>/dev/null');
            if ($result !== null) {
                return $result;
            }
            return "";
        case 'json':
            $error = $jsonLint->lint(file_get_contents($path));
            if ($error instanceof \Throwable) {
                return $error->getMessage();
            }
            return "";
        case 'css':
            if (!$cssLint->lintString(file_get_contents($path))) {
                // ignore unknown css property error
                foreach ($cssLint->getErrors() as $error) {
                    if (strpos($error, 'Unknown CSS property') === 0) {
                        continue;
                    }
                    return $error;
                }
                return "";
            }
            break;
    }
    return null;
}

function is_git_clean(string $repo): bool {
/*
    $cache = Cache::instance('git-status');
    $status = $cache->get($repo);
    if ($status !== null) {
        return $status;
    }
*/
    $status = '' === trim(shell_exec($cmd = "cd ".escapeshellarg($repo)."; git status --porcelain"));
    //$cache->set($repo, $status, 60*5);
    return $status;
}

/**
 * options:
 * 'name' => 'Test name'
 * 'stdout' => expected stdout
 * 'stderr' => expected stderr
 */
function run_test($path, array $options=[]) {
    global $cGOOD, $cNEUTRAL, $cNOTICE, $cBAD, $cWARNING, $cHIGHLIGHT, $cSTATUS, $cGFX, $cSTDOUT, $cSTDERR, $cOUTPUTMARK, $cH1, $cH2;
    global $term, $be_quiet, $be_brief;
    global $root;

    $filename = $path; //basename($path);
    if (!empty($options['filename'])) {
        $filename = $options['filename'];
    }
    if (!empty($options['name'])) {
        out_h2("Running test <!underline>".$options['name']."<!> from file <!underline>$filename<!>");
    } else {
        out_h2("Running test file <!underline>$filename<!>");
    }

    if (empty($options['stdout']) && file_exists($path.'.STDOUT')) {
        $options['stdout'] = file_get_contents($path.'.STDOUT');
    }

    if (empty($options['stderr']) && file_exists($path.'.STDERR')) {
        $options['stderr'] = file_get_contents($path.'.STDERR');
    }

    $cleanupFiles = [];
    $autoClean = new class($cleanupFiles) {
        public function __construct(&$cleanupFiles) {
            $this->cleanupFiles = &$cleanupFiles;
        }
        public function __destruct() {
            foreach ($this->cleanupFiles as $k => $fn) {
                if (file_exists($fn)) {
                    unlink($fn);
                }
                unset($this->cleanupFiles[$k]);
            }
        }
    };
    $cleanupFiles[] = $tempName = tempnam(sys_get_temp_dir(), 'charm-testing');
    $cleanupFiles[] = $errorFile = $tempName.'.log';
    $cleanupFiles[] = $outFile = $tempName.'.out';
    $cleanupFiles[] = $errFile = $tempName.'.err';

    // autoload.php is added automatically
    $cleanupFiles[] = $autoloadFile = tempnam(sys_get_temp_dir(), 'autoload');
    file_put_contents($autoloadFile, '<?'.'php
    require('.var_export($root.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php', true).');
');

    $cmd = 'exec '.PHP_BINARY.
        ' -d error_log='.escapeshellarg($errorFile).' -d zend.assertions=1 '.
        '-d assert.exception=1 -d hard_timeout=5 -d max_execution_time=10 -d assert.active=1 '.
        '-d assert.bail=0 -d display_errors=0 -d display_startup_errors=1 '.
        '-d error_reporting='.(E_ALL & ~E_DEPRECATED).' -d report_memleaks=1 '.
        '-d auto_prepend_file='.escapeshellarg($autoloadFile).' '.
        escapeshellarg(basename($path));

    $testStartTime = microtime(true);

    $forcedTimeout = false;

    $p = proc_open($cmd, [
        ['pipe', 'r'],
        ['pipe', 'w'],
        ['pipe', 'w']
    ], $pipes, dirname($path), getenv() + [ 'XDEBUG_MODE' => 'off' ] );

    $stdout = '';
    $stderr = '';

    if (is_resource($p)) {
        fwrite($pipes[0], '');
        fclose($pipes[0]);
        stream_set_blocking($pipes[1], false);
        stream_set_read_buffer($pipes[1], 0);
        stream_set_blocking($pipes[2], false);
        stream_set_read_buffer($pipes[2], 0);
        $outputStarted = false;
        $stdoutColor = $cSTDOUT;
        $stderrColor = $cSTDERR;
        $frame = 0;
        do {
            if (microtime(true) - $testStartTime > 11) {
                $forcedTimeout = true;
                proc_terminate($p);
            }
            if (!$be_quiet && $term->isTTY) {
                if (($frame++ % 500) === 0) {
                    $c = step_char();
                }
                tty_out("<!blue>$c<!>");
                $sleepTime = 100;
                if ($be_brief) {
                    tty_out(" \x1B[1D");
                }
//                out(" \x1B[1D");
            } else {
                $sleepTime = 1000;
            }
            $status = proc_get_status($p);
            $read = [ $pipes[1], $pipes[2] ];
            $void = [];
            $count = \stream_select($read, $void, $void, 0, $sleepTime);
            if (in_array($pipes[1], $read)) {
                $stdoutChunk = \stream_get_contents($pipes[1], 65536);
                if ($stdoutChunk !== '') {
                    $stdout .= $stdoutChunk;
                    if (!$outputStarted) {
                        $outputStarted = true;
                        $be_quiet or $be_brief or out(" Output from the test <!tty>(legend: $stdoutColor"."STDOUT<!> $stderrColor"."STDERR<!>)<!>\n$cGFX ╭────────────────────────────────────────────────\n │<!> ");
                    }
                    $be_quiet or $be_brief or out("$stdoutColor".strtr(Terminal::quote($stdoutChunk), [ "\n" => $cOUTPUTMARK."🔚<!>\n$cGFX │<!> " ]).'<!>');
                }
            }
            if (in_array($pipes[2], $read)) {
                $stderrChunk = \stream_get_contents($pipes[2], 65536);
                if ($stderrChunk !== '') {
                    $stderr .= $stderrChunk;
                    if (!$outputStarted) {
                        $outputStarted = true;
                        $be_quiet or $be_brief or out(" Output from the test <!tty>(legend: $stdoutColor"."STDOUT<!> $stderrColor"."STDERR<!>)<!>\n$cGFX ╭────────────────────────────────────────────────\n │<!> ");
                    }
                    $be_quiet or $be_brief or out("$stderrColor".strtr(str_replace("<!", "<\0!", $stderrChunk), [ "\r" => $cOUTPUTMARK."␍<!>\r$cGFX │<!> ", "\n" => $cOUTPUTMARK."🔚<!>\n$cGFX │<!> " ]).'<!>');
                }
            }
        } while ($status['running']);
        if ($outputStarted) {
            $be_quiet or $be_brief or out($cOUTPUTMARK."🔚<!>\n$cGFX ╰────────────────────────────────────────────────<!>\n\n");
        }
    } else {
        add_error($path, "Unable to execute test (cmd=$cmd)");
        out(" ".mark_error().$cBAD."Unable to run test!<!> Command: $cmd\n Exit code: $cBAD".$status['exitcode']."<!><!>\n\n");
        cleanup_files($cleanupFiles);
        return 1;
    }
    $exitCode = $status['exitcode'];

    $log = file_exists($errorFile) ? file_get_contents($errorFile) : '';

    if ($forcedTimeout) {
        add_error($path, "Process killed after timeout");
        out("\n");
        out_path_error($path, "Process killed after timeout");
        cleanup_files($cleanupFiles);
        return 2;
    }

    if (trim($log) !== '') {
        add_error($path, trim($log));
        if ($be_brief) {
            out_check_error("Errors were logged");
        } else {
            out_check_error("Errors were logged:");
            out_file($log);
        }
        cleanup_files($cleanupFiles);
        return 1;
    } else {
        add_success($path, "No logged errors");
        $be_brief or out_check_success("Error log was empty");
    }

    if (!empty($options['stdout'])) {
        if (rtrim($stdout) !== rtrim($options['stdout']) && null !== compare(rtrim($stdout), rtrim($options['stdout']))) {
            add_error($path, "STDOUT does not match expected output");
            if ($be_brief) {
                out_check_error("STDOUT did not match expected output");
            } else {
                out_check_error("STDOUT does not match expecation:");
                out(compare(rtrim($stdout), rtrim($options['stdout'])));
            }
            cleanup_files($cleanupFiles);
            return 1;
        } else {
            add_success($path, "STDOUT was validated");
            $be_brief or out_check_success("STDOUT was validated");
        }
    }

    if (!empty($options['stderr'])) {
        if (rtrim($stderr) !== rtrim($options['stderr']) && null !== compare(rtrim($stderr), rtrim($options['stderr']))) {
            add_error($path, "STDERR does not match expected output");
            if ($be_brief) {
                out_check_error("STDERR did not match expected output");
            } else {
                out_check_error("STDERR does not match expectation:");
                out(compare(rtrim($stderr), rtrim($options['stderr'])));
            }
            cleanup_files($cleanupFiles);
            return 1;
        } else {
            add_success($path, "STDERR was validated");
            $be_brief or out_check_success("STDERR was validate");
        }
    }

    if ($exitCode !== 0) {
        add_error($path, "Test exited with exit code $exitCode");
        out_check_error("Exit code was $exitCode, test failed");
        cleanup_files($cleanupFiles);
        return 1;
    } else {
        add_success($path, "Exit code was 0");
        $be_brief or out_check_success("Exit code was 0");
    }

    $be_brief or out("\n");

    cleanup_files($cleanupFiles);
    return 0;
}

function cleanup_files(array $files) {
    foreach ($files as $file) {
        if (file_exists($file)) {
            unlink($file);
        }
    }
}

function find_test_files($path) {

    foreach (find_paths($path, function($path, $basename) {
        $extension = strtolower(pathinfo($path, PATHINFO_EXTENSION));
        if ($basename === null) {
            return true;
        } elseif ($extension === 'php' || $extension === 'phpt') {
            return true;
        } else {
            return false;
        }
    }) as $path) {
        if (is_file($path)) {
            yield $path;
        }
    }
}

function find_dirs(string $path, string $dirname) {

    yield from find_paths($path, function($path, $basename) {
        return $basename === null;
    });
    return;
    foreach (find_paths($path) as $sub) {
        if (is_dir($sub) && basename($sub) === $dirname) {
            yield $sub;
        }
    }
    return;

}

/**
 ^ $checker is a callable which receives two arguments:
 * $fullPath and $basename.
 *
 * Return values from $checker:
 *
 * true         For directories: yield and recursively traverse
 *              For files: yield
 * false        For directories: don't yield and don't recurse
 *              For files: don't yield
 */
function find_paths(string $path, callable $checker=null) {
    if (is_file($path)) {
        if (!$checker || ($checker && $checker($path, basename($path)))) {
            yield $path;
        }
        return;
    }
    $subs = [];
    try {
        $d = @opendir($path);
        if (!$d) {
            throw new \Exception("Unable to open dir '$path'");
        }
        while (($file = readdir($d)) !== false) {
            if ($file === '.' || $file === '..') {
                continue;
            }
            $subs[] = $path . DIRECTORY_SEPARATOR . $file;
        }
        closedir($d);
        sort($subs);
    } catch (\Throwable $e) {
        add_notice($path, "Unable to open directory <!olive>$path<!>");
        return;
    }

    foreach ($subs as $sub) {
        if (is_dir($sub)) {
            if (!$checker || ($checker && $checker($sub, null))) {
                yield $sub;
                yield from find_paths($sub, $checker);
            }
        } else {
            if (!$checker || ($checker && $checker($sub, basename($sub)))) {
                yield $sub;
            }
        }
    }
}

/**
 * Iterate through a directory tree. For directories, the handler
 * can return false to prevent recursion.
 */
function walk_paths(string $path, callable $handler) {

    foreach (find_paths($path, function(string $path) use ($handler) {
        $result = $handler($path);
        if (is_dir($path) && $result === false) {
            return false;
        }
        return true;
    }) as $void) {
    };

}


function get_composer_json() {

}

function find_project_dir() {
    $path = getcwd();
    $limit = 10;
    while (!is_file($path.'/composer.json') && $limit-- > 0) {
        $path = dirname($path);
    }
    if ($limit === -1) {
        fail("Could not find a composer.json file in any of the parent directories");
    }
    return $path;
}

function tty_out(string $out) {
    global $term;
    if ($term->isTTY) {
        out($out);
    }
}

function stdout(string $out) {
    global $root;
    static $term = new Charm\Terminal(STDOUT);

    $out = strtr($out, [
        $root.'/' => '',
        $root.'\\' => '',
        $root => '.',
    ]);

    $term->write($out);
}

function fail(string $reason) {
    global $cHIGHLIGHT, $cGOOD, $cNEUTRAL, $cNOTICE, $cBAD;
    global $root;
    $root = '.';
    out("Failed: $cBAD$reason<!>\n\n");
    exit(1);
}

function notice(string $message) {
    out("$cHIGHLIGHT - $message<!>\n\n");
}

function compare($actual, $expected): ?string {
    global $cGOOD, $cNEUTRAL, $cNOTICE, $cBAD, $cWARNING, $cHIGHLIGHT, $cSTATUS, $cGFX, $cSTDOUT, $cSTDERR, $cOUTPUTMARK, $cH1, $cH2;

    static $term = new Charm\Terminal(STDOUT);

    $noProblems = true;

    $out = '';
    if ($actual instanceof \Throwable) {
        $a = (string) $actual;
    } elseif (!is_string($actual)) {
        $a = "[".php_encode($actual, true)."]";
    } else {
        $a = $actual;
    }
    if ($expected instanceof \Throwable) {
        $b = (string) $expected;
    } elseif (!is_string($expected)) {
        $b = "[".php_encode($expected, true)."]";
    } else {
        $b = $expected;
    }

    $resultColor = $a === $b ? $cGOOD : $cBAD;

/*
    $a = wordwrap($a);
    $b = wordwrap($b);
*/
    // remove any colors
    $a = preg_replace('#\\x1b[[][^A-Za-z]*[A-Za-z]#', '', $a);
    $b = preg_replace('#\\x1b[[][^A-Za-z]*[A-Za-z]#', '', $b);

    $aLines = explode("\n", strtr($a, ["\r\n" => "\n"]));
    $bLines = explode("\n", strtr($b, ["\r\n" => "\n"]));
    $aWidth = array_reduce($aLines, function($carry, $line) { return max($carry, mb_strlen($line)); }, 20);
    $bWidth = array_reduce($bLines, function($carry, $line) { return max($carry, mb_strlen($line)); }, 20);
    $result = [
        "+-".str_repeat("-", 5)."-+-".str_repeat("-", $aWidth)."-+-".str_repeat("-", $bWidth)."-+",
        "| ".$term->str_pad("<!white>Line<!>", 5)." | ".$term->str_pad("<!white>Result<!>", $aWidth)." | ".$term->str_pad("<!white>Expected Result<!>", $bWidth)." |",
        "+-".str_repeat("-", 5)."-+-".str_repeat("-", $aWidth)."-+-".str_repeat("-", $bWidth)."-+",
    ];

    $lineCount = max(count($aLines), count($bLines));
    $aSkew = 0;
    $bSkew = 0;
    for ($i = 0; $i < $lineCount; $i++) {
        $aLine = $aLines[$i] ?? null;
        $bLine = $bLines[$i] ?? null;
        $similarity = '';

        $matched = false;
        if (($aLines[$i + $aSkew] ?? null) === ($bLines[$i + $bSkew] ?? null)) {
            $matched = true;
        }
        if (!$matched && isset($bLines[$i + $bSkew])) {
            for (;;) {
                $token = mt_rand(1000000, 9999999);
                if (strpos($b, $token) === false) {
                    break;
                }
            }
            $linePattern = strtr($bLines[$i + $bSkew], [
                "?" => $token."SINGLE",
                "*" => $token."MULTIPLE",
            ]);
            $linePattern = preg_quote($linePattern, "/");
            $linePattern = strtr($linePattern, [
                $token."SINGLE" => ".",
                $token."MULTIPLE" => ".+",
            ]);
            $matched = isset($aLines[$i+$aSkew]) && preg_match('/^'.$linePattern.'$/', $aLines[$i + $aSkew], $matches);
            $matched = !!$matched;
        }

        if ($matched) {
            // do nothing when lines match
        } elseif (($aLines[$i + $aSkew] ?? null) === null) {
            $noProblems = false;
            $bLine = "$cBAD$bLine<!>";
        } elseif (($bLines[$i + $bSkew] ?? null) === null) {
            $noProblems = false;
            $aLine = "$cBAD$aLine<!>";
        } else {
            $noProblems = false;
            similar_text($aLines[$i+$aSkew], $bLines[$i+$bSkew], $similarity);
            $aLine = "$cBAD$aLine<!>";
            $bLine = "$cGOOD$bLine<!>";
            if ($similarity < 10) {
                $bSkew--;
            }
        }
        $result[] =
            "| ".$term->str_pad((string) $i+1, 5, ' ', STR_PAD_LEFT).
            " | ".$term->str_pad($aLine ?? '', $aWidth).
            " | ".$term->str_pad($bLine ?? '', $bWidth).
            " |";
    }

    $result[] = $result[0];
    $out .= ($term->isTTY ? "\r".$term->clearRight() : "").implode("\n", $result)."\n\n";

    if ($noProblems) {
        return null;
    }

    return $out;

    if (is_array($actual)) {
        $phpArray = php_encode($actual);
        $out .= <<<RES
            Result value:
            $cHIGHLIGHT$phpArray<!>
            RES;
        $out .= "\n array: `<!white>".strtr(json_encode($actual), [ "\/" => "/" ])."<!>`\n\n";
        return $out;
    }
    return '';
}

function add_error(string $path, string $message) {
    global $errorList, $errors;
    $errorList[] = [ $path, $message, 'error' ];
    $errors++;
}

function add_warning(string $path, string $message) {
    global $errorList, $warnings;
    $errorList[] = [ $path, $message, 'warning' ];
    $warnings++;
}

function add_notice(string $path, string $message) {
    global $errorList, $notices;
    static $seen = [];
    if (key_exists($path, $seen)) {
        return;
    }
    out(" <!white>><!> <!silver>$message<!>\n");
    $seen[$path] = true;
    $errorList[] = [ $path, $message, 'notice' ];
    $notices++;
}

function add_success(string $path, string $message) {
    global $errorList, $successes;
    $errorList[] = [ $path, $message, 'success' ];
    $successes++;
}

function step_char() {
    static $i = 0;
    $pattern = [
        '⠁',
        '⠃',
        '⠇',
        '⡇',
        '⣆',
        '⣤',
        '⣰',
        '⢸',
        '⠹',
        '⠛',
        '⠏',
        3,
    ];
    if (!isset($pattern[$i])) {
        // loop
        $pattern[$i] = 0;
    }
    if (is_int($pattern[$i])) {
        // goto
        $i = $pattern[$i];
    }
    return $pattern[$i++]."\x1B[1D";
}

function out(string $out, bool $not_brief=false) {
    global $root, $term, $be_quiet, $be_brief;

    if ($be_quiet) {
        return;
    }
    if ($be_brief && $not_brief) {
        return;
    }

    $out = strtr($out, [
        $root.'/' => '',
        $root.'\\' => '',
        $root => '.',
    ]);

    $term->write($out);
    return;
}

function out_h1(string $message) {
    global $cH1, $root, $term, $be_brief;

    $message = strtr($message, [
        $root.'/' => '',
        $root.'\\' => '',
        $root => '.',
    ]);

    $res = $cH1.$message."<!>\n";
    if (!$be_brief) {
        $res .= $cH1.str_repeat("═", Terminal::strlen($message))."<!>\n";
    }

    out($res."\n");
}

function out_h2(string $message) {
    global $cH2, $root, $term, $be_brief;

    $message = strtr($message, [
        $root.'/' => '',
        $root.'\\' => '',
        $root => '.',
    ]);

    $res = $cH2.$message."<!>\n";
    if (!$be_brief) {
        $res .= $cH2.str_repeat("─", Terminal::strlen($message))."<!>\n";
    }

    out($res."\n");
}

function out_file(string $message) {
    global $cGFX, $cOUTPUTMARK;
    out("$cGFX ╭────────────────────────────────────────────────\n │<!> ");
    out(strtr($message, ["\r" => $cOUTPUTMARK."␍<!>\r$cGFX │<!> ", "\n" => $cOUTPUTMARK."🔚<!>\n$cGFX │<!> "])."\n");
    out("$cGFX ╰────────────────────────────────────────────────<!>\n\n" );
}

function out_check_success(string $message) {
    global $outStack;
    if (!empty($outStack)) {
        ++$outStack[count($outStack)-1][1];
    }
    out(" ".mark_success()." $message\n");
}

function out_check_warning(string $message) {
    global $outStack;
    if (!empty($outStack)) {
        ++$outStack[count($outStack)-1][1];
    }
    out(" ".mark_warning()." $message\n");
}

function out_check_error(string $message) {
    global $outStack;
    if (!empty($outStack)) {
        ++$outStack[count($outStack)-1][1];
    }
    out(" ".mark_error()." $message\n");
}

function out_summary(string $intro, array $results) {
    out(" $intro\n\n");
    out_start("summary");
    foreach ($results as $result) {
        if ($result[1] === 0) {
            continue;
        }
        switch ($result[0]) {
            case 'success':
                out_summary_success(string_interpolate($result[2], [ 'count' => $result[1] ]));
                break;
            case 'warning':
                out_summary_warning(string_interpolate($result[2], [ 'count' => $result[1] ]));
                break;
            case 'error':
                out_summary_error(string_interpolate($result[2], [ 'count' => $result[1] ]));
                break;
            default:
                out_summary_info(string_interpolate($result[2], [ 'count' => $result[1] ]));
                break;
        }
    }
    out_end("summary");
}

function out_path_info(string $path, string $message) {
    global $cHIGHLIGHT, $cNEUTRAL, $outStack;
    if (!empty($outStack)) {
        ++$outStack[count($outStack)-1][1];
    }
    out(" ".mark_info()." $cHIGHLIGHT$path<!> $cNEUTRAL$message<!>\n");
}

function out_path_success(string $path, string $message) {
    global $cHIGHLIGHT, $cGOOD, $outStack;
    if (!empty($outStack)) {
        ++$outStack[count($outStack)-1][1];
    }
    out(" ".mark_success()." $cHIGHLIGHT$path<!> $cGOOD"."$message<!>\n");
}

function out_path_warning(string $path, string $message) {
    global $cHIGHLIGHT, $cNOTICE, $outStack;
    if (!empty($outStack)) {
        ++$outStack[count($outStack)-1][1];
    }
    out(" ".mark_warning()." $cHIGHLIGHT$path<!> ".$cNOTICE."$message<!>\n");
}
function out_path_error(string $path, string $message) {
    global $cHIGHLIGHT, $cBAD, $outStack;
    if (!empty($outStack)) {
        ++$outStack[count($outStack)-1][1];
    }
    out(" ".mark_error()." $cHIGHLIGHT$path<!> $cBAD$message<!>\n");
}
function out_summary_info(string $message) {
    global $cHIGHLIGHT, $cNEUTRAL, $outStack;
    if (!empty($outStack)) {
        ++$outStack[count($outStack)-1][1];
    }
    out(" ".mark_info()." $cNEUTRAL$message<!>\n");
}
function out_summary_success(string $message) {
    global $cHIGHLIGHT, $cGOOD, $outStack;
    if (!empty($outStack)) {
        ++$outStack[count($outStack)-1][1];
    }
    out(" ".mark_success()." $cGOOD$message<!>\n");
}
function out_summary_warning(string $message) {
    global $cHIGHLIGHT, $cNOTICE, $outStack;
    if (!empty($outStack)) {
        ++$outStack[count($outStack)-1][1];
    }
    out(" ".mark_warning()." $cNOTICE$message<!>\n");
}
function out_summary_error(string $message) {
    global $cHIGHLIGHT, $cBAD, $outStack;
    if (!empty($outStack)) {
        ++$outStack[count($outStack)-1][1];
    }
    out(" ".mark_error()." $cBAD$message<!>\n");
}
function out_conclude_success(string $message) {
    global $cGOOD;
    out($cGOOD." ".$message."<!>\n\n");
}
function out_conclude_warning(string $message) {
    global $cWARNING;
    out($cWARNING." ".$message."<!>\n\n");
}
function out_conclude_error(string $message) {
    global $cBAD;
    out($cBAD." ".$message."<!>\n\n");
}
function out_start(string $type) {
    global $outStack;
    $outStack[] = [$type, 0];
}
function out_end(string $type) {
    global $outStack;
    if (empty($outStack)) {
        throw new \LogicException("Output stack is empty");
    } elseif ($outStack[count($outStack)-1][0] !== $type) {
        throw new \LogicException("Output stack mismatch: Unclosed ".$outStack[count($outStack)-1][0]);
    }
    $top = array_pop($outStack);
    if ($top[1] !== 0) {
        out("\n");
    }
}
function string_interpolate(string $text, array $vars) {
    $map = [];
    foreach ($vars as $key => $val) {
        if (is_bool($val)) {
            $val = json_encode($val);
        } elseif (is_null($val)) {
            $val = 'NULL';
        } elseif (is_array($val) && array_filter($val, is_scalar(...)) === $val) {
            $val = implode("<!>, <!bold>", $val);
        } elseif (is_scalar($val)) {
            $val = (string) $val;
        } else {
            continue;
        }
        $map['{'.$key.'}'] = "<!bold>$val<!>";
    }
    return strtr($text, $map);
}
function mark_success(): string {
    global $cGOOD;
    return $cGOOD."✅<!>";
}
function mark_warning(): string {
    global $cWARNING;
    return $cWARNING."😯<!>";
}
function mark_error(): string {
    global $cBAD;
    return $cBAD."❌<!>";
}
function mark_info(): string {
    global $cHIGHLIGHT;
    return $cHIGHLIGHT."👓<!>";
}
function mark_unknown(): string {
    return "❓";
}
function mark_dot(): string {
    return "⭕";
}
