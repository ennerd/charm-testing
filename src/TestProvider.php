<?php
namespace Charm\Testing;

abstract class TestProvider {

    /**
     * Lines to append when listing usage options in the form of
     * [
     *     [ 'x', ['some-long'], 'Some description' ],
     * ]
     */
    public static array $usageOptions;

    public function run(): void;

}
