<?php
namespace Charm\Testing;

class Cache {

    private $root;
    private $ns;
    private $matrix;

    public static function instance(string $ns=null): Cache {
        return new Cache(Env::varPath() . '/cache', $ns);
    }

    public function __construct(string $root, string $ns=null) {
        if ($ns === null) {
            $ns = 'default';
        }
        $this->root = realpath($root);
        $this->ns = $ns;
        $this->matrix = 2;
    }

    public function flush(string $ns=null): void {
        if ($ns === null) {
            $path = $this->root . DIRECTORY_SEPARATOR . "*";
        } else {
            $path = $this->root . DIRECTORY_SEPARATOR . $ns . DIRECTORY_SEPARATOR . "*";
        }
        if (!$this->isSafePath($path)) {
            throw new \Exception("Unsafe path '$path'");
        }
        shell_exec("rm -rf ".escapeshellarg($this->root . DIRECTORY_SEPARATOR . "*"));
    }

    public function get(string $key, mixed $default=null): mixed {
echo "CACHE GET $key\n";
        $path = $this->path($key);
        if (!is_file($path)) {
            return $default;
        }
        $entry = json_decode(file_get_contents($path));
        if ($entry->expires < microtime(true)) {
            unlink($path);
            return $default;
        }
        return $entry->value;
    }

    public function set(string $key, mixed $value, float $ttl): void {
        $entry = (object) [ 'key' => $key, 'value' => $value, 'expires' => microtime(true) + $ttl ];
        file_put_contents($this->path($key, true), json_encode($entry));
    }

    private function isSafePath(string $path): bool {
        $real = realpath($path);
        if (strpos($this->root, $real) === 0) {
            return true;
        }
        return false;
    }

    private function path(string $key, bool $mkdir=false): string {
        $path = $this->root . DIRECTORY_SEPARATOR . $this->ns . DIRECTORY_SEPARATOR . $this->hash($key);
        if ($mkdir) {
            $dir = dirname($path);
            mkdir($dir, 0700, true);
        }
        return $path;
    }

    private function hash(string $key): string {
        $hash = md5($key);
        $parts = [];
        for ($i = 0; $i < $this->matrix; $i++) {
            $parts[] = substr($hash, 0 + ($i * 2), 2);
        }
        $parts[] = $hash;
        return implode(DIRECTORY_SEPARATOR, $parts);
    }

}
